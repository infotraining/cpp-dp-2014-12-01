#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include <memory>
#include "clone_factory.hpp"

namespace Drawing
{

using ShapePtr = std::shared_ptr<Shape>;

class ShapeGroup : public Shape
{
    std::vector<ShapePtr> shapes_;
public:
    ShapeGroup() = default;
    
    ShapeGroup(const ShapeGroup& sg)
    {
        for(const auto& shape : sg.shapes_)
            shapes_.emplace_back(shape->clone());
    }
    
    void draw() const override
    {
        for(const auto& shape : shapes_)
            shape->draw();
    }
    
    void move(int dx, int dy) override
    {
        for(const auto& shape : shapes_)
            shape->move(dx, dy);
    }
    
    ShapeGroup* clone() const override
    {
        return new ShapeGroup(*this);
    }

    void read(std::istream& in) override
    {
        int count;
        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string type_identifier;
            in >> type_identifier;

            auto shape = ShapeFactory::instance().create(type_identifier);
            shape->read(in);

            shapes_.emplace_back(shape);
        }
    }

    void write(std::ostream& out) override
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;

        for(const auto& shape : shapes_)
            shape->write(out);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
