#include "shape_group.hpp"

namespace
{
    using namespace Drawing;

    bool is_registred = ShapeFactory::instance().register_shape("ShapeGroup", new ShapeGroup());
}
