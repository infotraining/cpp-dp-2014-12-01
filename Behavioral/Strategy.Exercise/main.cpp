#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

typedef std::vector<StatResult> Results;

enum StatisticsType
{
	AVG, MINMAX, SUM
};

class DataAnalyzer
{
	StatisticsType stat_type_;
	std::vector<double> data_;
public:
	DataAnalyzer()
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

	void save_data(const std::string& file_name) const
	{ 
		std::ofstream fout(file_name.c_str());
		if (!fout)
			throw std::runtime_error("File not opened");

		for(std::vector<double>::const_iterator it = data_.begin(); it != data_.end(); ++it)
			fout << (*it) << std::endl;
	}

	void set_statistics(StatisticsType stat_type)
	{
		stat_type_ = stat_type;
	}

	void calculate(Results& results)
	{
		if (stat_type_ == AVG)
		{
			double sum = std::accumulate(data_.begin(), data_.end(), 0.0);
			double avg = sum / data_.size();

			StatResult result("AVG", avg);
            results.push_back(result);
		}
		else if (stat_type_ == MINMAX)
		{
			double min = *(std::min_element(data_.begin(), data_.end()));
			double max = *(std::max_element(data_.begin(), data_.end()));

            results.push_back(StatResult("MIN", min));
            results.push_back(StatResult("MAX", max));
		}
		else if (stat_type_ == SUM)
		{
			double sum = std::accumulate(data_.begin(), data_.end(), 0.0);

            results.push_back(StatResult("SUM", sum));
		}
	}
};

void print_results(const Results& results)
{
    for(Results::const_iterator it = results.begin(); it != results.end(); ++it)
		std::cout << it->description << " = " << it->value << std::endl;
}

int main()
{
	Results results;

	DataAnalyzer da;
	da.load_data("data.dat");

	da.set_statistics(AVG);
	da.calculate(results);

	da.set_statistics(MINMAX);
	da.calculate(results);

	da.set_statistics(SUM);
	da.calculate(results);

	print_results(results);

	std::cout << "\n\n";

	results.clear();
	da.load_data("new_data.dat");
	da.calculate(results);

	print_results(results);
}
